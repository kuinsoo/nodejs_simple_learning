# HTTP Module
Node.js 강좌 002_server 에서 맛보기로 Hello World 만을 리턴하는 웹서버를 만들어봐었습니다.\
이번에는 http 모듈을 이용해 더 기능이 향상된 웹서버와 웹 클라이언트를 코딩해보겠습니다.

# HTTP 서버 예제
우선 index.html을 생성하세요.
```html
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Sample Page</title>
</head>
<body>
   <h1>Hello World!</h1>
</body>
</html>
```
다음 server.js 를 작성
```javascript
var http = require('http');
var fs = require('fs');
var url = require('url');


// 서버 생성
http.createServer( function (request, response) {  
   // URL 뒤에 있는 디렉토리/파일이름 파싱
   var pathname = url.parse(request.url).pathname;
   
   
   console.log("Request for " + pathname + " received.");
   
   // 파일 이름이 비어있다면 index.html 로 설정
   if(pathname=="/"){
       pathname = "/007_HTTP_Module/index.html";
   }
   
   // 파일을 읽기
   fs.readFile(pathname.substr(1), function (err, data) {
      if (err) {
         console.log(err);
         // 페이지를 찾을 수 없음
         // HTTP Status: 404 : NOT FOUND
         // Content Type: text/plain
         response.writeHead(404, {'Content-Type': 'text/html'});
      }else{	
         // 페이지를 찾음	  
         // HTTP Status: 200 : OK
         // Content Type: text/plain
         response.writeHead(200, {'Content-Type': 'text/html'});	
         
         // 파일을 읽어와서 responseBody 에 작성
         response.write(data.toString());		
      }
      // responseBody 전송
      response.end();
   });   
}).listen(8081);


console.log('Server running at http://127.0.0.1:8081/');
```
클라이언트에서 서버에 접속을하면 URL 에서 열고자 하는 파일을 파싱하여 열어줍니다.\
파일이 존재하지 않는다면 콘솔에 에러 메시지를 출력합니다.

# 출력물
```
Server running at http://127.0.0.1:8081/
Request for / received.
Request for /showmeerror received.
{ [Error: ENOENT: no such file or directory, open '파일경로\nodejs\showmeerror']
  errno: -4058,
  code: 'ENOENT',
  syscall: 'open',
  path:
   '파일경로\\nodejs\\showmeerror' }
Request for /007_HTTP_Module/index.html received.
```
서버를 실행하고 다음 링크들을 들어갔을때 뜨는 출력물입니다 \
http://127.0.0.1:8081/ \
http://127.0.0.1:8081/showmeerror \
http://127.0.0.1:8081/007_HTTP_Module/index.html

# HTTP 클라이언트 예제
```javascript
var http = require('http');

// HTTPRequest의 옵션 설정
var options = {
   host: 'localhost',
   port: '8081',
   path: '/007_HTTP_Module/index.html'  
};

// 콜백 함수로 Response를 받아온다
var callback = function(response){
   // response 이벤트가 감지되면 데이터를 body에 받아온다
   var body = '';
   response.on('data', function(data) {
      body += data;
   });
   
   // end 이벤트가 감지되면 데이터 수신을 종료하고 내용을 출력한다
   response.on('end', function() {
      // 데이저 수신 완료
      console.log(body);
   });
}
// 서버에 HTTP Request 를 날린다.
var req = http.request(options, callback);
req.end();
```
14번과 19번 줄을 보면 response.on() 을 사용하였습니다. .on() 메소드, 익숙하지 않은가요?\
response 는 006_EventLoop 에서 봤었던 EventEmitter 클래스를 상속한 객체 입니다.

# 출력물
```html
<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>Sample Page</title>
</head>
<body>
   <h1>Hello World!</h1>
</body>
</html>
```