# Event Loop
Node.js 에선 Event 를 매우 많이 사용하고, 이 때문에 다른 비슷한 기술들보다 훨씬 빠른 속도를 자랑합니다.\
Node.js 기반으로 만들어진 서버가 가동되면,  변수들을 initialize 하고, 함수를 선언하고 이벤트가 일어날때까지 기다립니다.\
이벤트 위주 (Event-Driven) 어플리케이션에서는, 이벤트를 대기하는 메인 루프가 있습니다.\
그리고 이벤트가 감지되었을시 Callback 함수를 호출합니다.\

<center>

![EventLoop](EventLoop.png)

</center>
이벤트가 콜백과 비슷해보일 수 도 있습니다. 차이점은, 콜백함수는 비동기식 함수에서 결과를 반환할때 호출되지만, 이벤트 핸들링은 옵저버 패턴에 의해 작동됩니다.

***
<center>
   옵저버 패턴 

   디자인 패턴 중 하나입니다. 자세한 내용은 <a href="https://ko.wikipedia.org/wiki/%EC%98%B5%EC%84%9C%EB%B2%84_%ED%8C%A8%ED%84%B4" >위키피디아</a>를 참조하세요.
</center>

***
이벤트를 대기하는 (EventListeners) 함수들이 옵저버 역활을 합니다. 옵저버들이 이벤트를 기다리다가, 이벤트가 실행되면 이벤트를 처리하는 함수가 실행됩니다.
Node.js 에는 events 모듈과 EvenEmitter 클래스가 내장되어 있습니다.
이를 사용하여 이벤트와 이벤트 핸들러를 연동(bind) 시킬 수 있습니다.
```javascript
// events 모듈 사용
var events = require('events');

// EventEmitter 객체 생성
var eventEmitter = new events.EventEmitter();
```
이벤트 핸들러와  이벤트를 연동시키는건 다음과 같습니다.
```javascript
// event 와 EventHandler 를 연동(bind)
// eventName 은 임의로 설정 가능
eventEmitter.on('eventName', eventHandler);
```
프로그램안에서 이벤트를 발생시킬땐 다음코드를 사용합니다. 
```javascript
eventEmitter.emit('eventName');
```

# 이벤트 핸들링 예제
위에서 배운것을 토대로 이벤트를 다루는 예제를 작성해보도록 하겠습니다. ex) EventLoop.js
```javascript
// event 모듈 사용
var evnets = require('events');

// EventEmitter 객체 생성
var eventEmitter = new evnets.EventEmitter();

// EventHandler 함수 생성
var connectHandler = function connected() {
   console.log('Connection Successful');

   // data_received 이벤트를 발생시키기
   eventEmitter.emit("data_received");
}

// connection 이벤트와 connectHandler 이벤트 핸들러를 연동
eventEmitter.on('connection', connectHandler);

// data_received 이벤트와 익명 함수와 연동
// 함수를 변수안에 담는 대신에, .on() 메소드의 인자로 직접 함수를 전달
eventEmitter.on('data_received', function() {
   console.log("Data  Received");
});


// conection 이벤트 발생시키기
eventEmitter.emit('connection');

console.log('Program has ended');
```

# 출력물
```
$ node main.js
Connection Successful
Data Received
Program has ended
```

> ## EventEmitter 메소드
> EventEmitter 의 상세한 내용은 <a href="https://nodejs.org/api/events.html#events_class_eventemitter">NodeJS Documentation</a>을 확인해주세요.